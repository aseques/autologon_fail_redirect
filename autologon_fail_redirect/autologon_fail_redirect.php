<?php

/**
 * Automatic login + redirect in case a SQL condition is matched (such as
 *  account not yet validated)
 * Otherwise it acts exactly as autologon
 */
class autologon_fail_redirect extends rcube_plugin
{
  public $task = 'login';

  function init()
  {
    $this->add_hook('startup', array($this, 'startup'));
    $this->add_hook('authenticate', array($this, 'authenticate'));
  }

  function startup($args)
  {
    $rcmail = rcmail::get_instance();

    // change action to login
    if (empty($_SESSION['user_id']) && !empty($_POST['_autologin']) && $this->is_localhost())
      $args['action'] = 'login';
    $this->load_config();
    return $args;
  }


  function authenticate($args)
  {
    $rcmail = rcmail::get_instance();
    if (!empty($_POST['_autologin'])) {
      $args['user'] = $_POST["_user"].'@'.$_POST["_domain"];
      $args['pass'] = $_POST["_pass"];
      $args['host'] = 'localhost';
      $args['cookiecheck'] = false;
      $args['valid'] = true;
      $domain=$_POST["_domain"];
    }
 
    if (!($sql = $rcmail->config->get('password_query'))) {
	write_log('errors', 'autologon_fail_redirect: password_query value couldn\'t be load from the configuration');
	return false;
    }

    if ($dsn = $rcmail->config->get('password_db_dsn')) {
	$db = new rcube_mdb2($dsn, '', FALSE);
	$db->set_debug((bool)$rcmail->config->get('sql_debug'));
	$db->db_connect('w');
    } 
    else {
	return false;
    }

    // we replace the user and domain into the SQL query
    $sql = str_replace('%l', $db->quote($_POST["_user"], 'text'), $sql);
    $sql = str_replace('%d', $db->quote($_POST["_domain"], 'text'), $sql);
    $res = $db->query($sql, $sql_vars);

    if (!$db->is_error()) {
	    if ($result = $db->fetch_array($res)) {
		setcookie ('ajax_login','',time()-3600);
		//We use the data from sql to return as appropiate
		$redirect='http://'.$domain.'/?user='.$_POST["_user"].'&active='.$result[0];
		console('Redirecting to '.$redirect.' because condition matched');
 		$rcmail->output->add_script('top.location.href="' . $redirect.'"');
		$args = false;
	    }
    }
    return $args;
  }

  function is_localhost()
  {
    return true;
    //return $_SERVER['REMOTE_ADDR'] == '::1' || $_SERVER['REMOTE_ADDR'] == '127.0.0.1';
  }
}

